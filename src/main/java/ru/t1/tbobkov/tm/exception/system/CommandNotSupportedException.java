package ru.t1.tbobkov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Argument not supproted...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ''" + command + "'' not supported...");
    }


}
